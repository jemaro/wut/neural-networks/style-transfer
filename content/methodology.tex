\section{Methodology}
\label{sec:methodology}

Before the first approaches of Neural Style Transfer other methods were used to
perform artistic stylization. A popular one used in computer graphics is 
non-photorealistic rendering, which dates back to 1990 with a first approach by
Paul Haebreli \cite{PaintNumbers1990}.

Improvements in non CNN-based approaches continued during the past decades,
having the first deep neural network approach published in 2015 by
\citet{gatys2015neural}. Their system uses neural transfer styling (NTS)
separating the content and style of the sample images for the creation of
artistic pictures. 
% The theory of the implementation of their transfer learning
% network will be based on their original paper.

The publication presents how the image content and texture can be separated
from each other, so that the content of one image, called \emph{content image},
can be represented using the style of another image, called \emph{style image}.

Relevant issues of this algorithm of implementing a neural transfer network
were addressed in 2017 by \citet{NNStyleTransferReview2017}. The technique
fails to preserve the coherence of fine structures, it also doesn’t consider
the variations of brush strokes, and the semantics and depth information
contained in the content image, which are important factors in evaluating the
visual quality.

\subsection{Architecture}
The underlying architecture of the style transfer network is based on
Convolutional Neural Networks. In the developed model by \citet{VGG19} the
VGG19 CNN is used, which introduces the importance of prioritizing depth over
larger filter kernels for better performance. However, the last layer used for
classification is removed, as even though there will be a loss function for the
content in the image, there should be no output regarding the class of the
object contained in the picture. It is also important to consider that the
model of the VGG19 network will not be trained, and the pretrained weights will
be considered for this project in NTS. The backpropagation and gradient descent
mentioned in the following sections will be related to adjusting an initial
random image to the the content and style samples.

The project has been tested using the VGG19 network as the proposed base of the
algorithm. This network provides simplicity and efficiency just consisting of
19 layers in a sequence of convolution, ReLu, and max pooling. 

\subsection{Initialization}
The initialization of the network proposed in the paper is to start with a
white noise image, in which gradient descent will be performed. The pixels will
continuously rearrange based on both components of the loss, until the maximum
number of iterations is reached. However, an alternative to this can be to
consider the content image as the input image, which will result in an
adaptation based mainly on the style component. 

\subsection{Content feature extraction}
The content from the \emph{content image} is extracted using the CNN’s feature
extraction properties. Each layer can be considered as a group of filters, and
each of these groups will extract a certain feature from the input image. The
deeper the layer in the network, the higher the level of the content will be.
Meanwhile, the first layers of the network will be extracting more specific
features, such as lines or corners. 

\subsection{Style feature extraction}
Regarding the style of the image, the adapt a method from a previous work
\emph{Texture Synthesis using Convolutional Neural Networks}
\cite{TextureSynthesis2015}, where texture features are obtained by computing
the correlation between the different filter responses. 

\subsection{Loss function}
The loss function is formed by two components, the content loss and the style
loss. Combined with some predefined weights form the total loss function
described in \autoref{eqn:total-loss}.

\begin{equation}
    \mathcal{L}_{total}(\vec{p},\vec{a},\vec{x}) =
    \alpha\mathcal{L}_{content}(\vec{p},\vec{x})
    + \beta\mathcal{L}_{style}(\vec{a},\vec{x})
    \label{eqn:total-loss}
\end{equation}

Where $\vec{p}$ and $\vec{x}$ correspond to the original image, and the
generated image respectively.

\subsubsection{Content loss}

Content loss in layer $l$ is defined by the squared error loss between their
feature representations as described by \autoref{eqn:content-loss}. One can
also find its derivative in \autoref{eqn:content-loss-derivative}.

\begin{equation}
    \mathcal{L}_c = \sum_{l\in \{l_c\}}
    \parallel\mathcal{F}^l(I_c) - \mathcal{F}^l(I)\parallel^2 
    \label{eqn:content-loss}
\end{equation}

\begin{equation}
    \frac{\partial\mathcal{L}_{content}}{\partial F_{ij}^l} = 
    \begin{cases} 
        (F^l - P^l) & \text{if } F_{ij}^l > 0 \\
        0 & \text{if } F_{ij}^l < 0
    \end{cases}
    \label{eqn:content-loss-derivative}
\end{equation}

Where $\mathcal{F}^l(I_c)$ and $\mathcal{F}^l(I)$ their feature representation
on layer $l$. Note that the addition is performed only in the set of selected
layers for the computation of the content loss. The proposed method in the
paper chooses a set $l_c$ of just one element.
%  WE CAN COMPARE THIS AND SEE RESULTS MAYBE with a few more images in the content set

Backpropagation is used with gradient descent to change the initial random
image $x$ to imitate the original image $p$. In other words, instead of using
the content and the style images in separate pipelines through the network, the
CNN goes back to the random image and modifies it according to the computed
losses.

\subsubsection{Style loss}

The method for computing the style loss is more complex. Correlations between
filter responses in different layers of the pretrained VGG19 network are
processed using a Gram-based representation to model textures. The feature map
of layer $l$ is represented by $\mathcal{F}^l$ and the Gram matrix for layer
$l$ between feature maps $i$ and $j$ is given by \autoref{eqn:gram-matrix}.

\begin{equation}
    G_{ij}^l = \sum_k F_{i k}^l F_{j k}^l 
    \label{eqn:gram-matrix}
\end{equation}

Optimization of the style loss function is performed by minimizing the square
distance between the Gram-based style representations of both images. The
layer’s contribution to the total loss is defined by \autoref{eqn:style-loss}

\begin{equation}
    \mathcal{L}_s = \sum_{l \in \{l_s\}}
    \parallel \mathcal{G}(\mathcal{F}^l(I_s)') 
    - \mathcal{G}(\mathcal{F}^l(I)') \parallel^2
    \label{eqn:style-loss}
\end{equation}

Note again that the addition is performed only in the set of selected layers
for the computation of the style loss. 

It is in the scope of this project to perform variations in the elements of
sets $l_c$ and $l_s$ and observe the results, as well as changing the weights of
each component in the loss function for style and content. 

\subsection{Optimizer}
The optimizer used will be the Adaptive Moment Optimization Algorithm (Adam)
\cite{kingma2017adam}. It is based on an efficient way of performing stochastic
gradient descent using the first and second order moments. It combines the
gradient descent with momentum, for the first order momentum, and the RMS
technique, based on the second order momentum. The parameters of the objective
function will be updated in each iteration of the loop. 

Adam will perform an initial quick optimization initially and focus on reaching
a local minimum, and slow down to perform precise optimization the closer it
gets.

Three parameters of the Adam optimizer can be configured:

\begin{itemize}
    \item $\beta_1$: Refers to the exponential decay rate of the first order
    moments. Between 0 and 1.
    \item $\beta_2$: Refers to the exponential decay rate of the second order
    moments. Between 0 and 1.
    \item $\epsilon$: Prevents division by zero in any operation of the optimizer.
\end{itemize}

% The first and second moments update rule is given by: 

% gt is the gradient of the function at timestep t

