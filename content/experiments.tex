\section{Experiments}

Several experiments where carried out with the Neural Style Transfer
implementation described in \autoref{sec:implementation}.
\autoref{table:defaults} shows the parameters used unless otherwise specified
for the particular experiment. The default parameters have been induced
directly from \tutorialref. The runtime environment for all experiments is a
google colab instance with GPU accelerated hardware.

\input{content/experiments/table_defaults}

\subsection{Deepness of the layers}
The shallowness in the network will have a big impact on the types of features
that are being extracted. If the layer is deep, the extraction will be of
high-level features, and if the layer is shallow, the features will be
low-level. \autoref{table:deepness-definitions} describes which layers will be
considered as deep or shallow for each, content and style.

\begin{table}
    \centering
    \begin{tabular}{c|c|c|}
        & Content & Style \\
        \hline
        Deep & \verb|block5_conv2| 
            & \verb|block4_conv3|, \verb|block5_conv3|, \verb|block5_conv4| \\
        \hline
        Neutral & \verb|block3_conv2| 
            & \verb|block3_conv1|, \verb|block3_conv3|, \verb|block4_conv1| \\
        \hline
        Shallow & \verb|block1_conv2| 
            & \verb|block1_conv1|, \verb|block2_conv1|, \verb|block2_conv2| \\
        \hline
    \end{tabular}
    \caption{
        Definitions of deep, neutral and shallow layers for content and style
        feature extraction
    }
    \label{table:deepness-definitions}
\end{table}

As one can observe in \autoref{table:deepness-comparison}, this is
more obvious in the shallow style layers, where the extracted low level
features of the waves make it difficult to see the wave shape in the output
image. Meanwhile in the deep style layers, the wave shapes are more clearly
visible. If the comparison is done for content layers, the  visual output is no
so obvious due to the fact that the input image to the network was the content
image.

\input{content/experiments/table_deepness}

\subsection{Amount of layers}

The number of content and style layers also plays an important role in the
definition of the output. But it is difficult to study its effects as one can't
add more layers without inducing the already mentioned effects of deepness of
the layers. 

\autoref{table:amount-of-style-layers} shows that if only one layer of each is
selected, the output image results in an inaccurate combination of the style
and content images. The key point here is to improve the results by selecting
more than one style layers. This will lead to a multiscale computation of the
loss functions leading to smoother results. 

However, it makes little sense to use many layers as the results will stop
improving noticeably after three or four layers have been selected, and
increasing the number of layers will require more loss function computations,
which will increase the computation time. Incrementing the number of content
layers did not had an impact in the output image but it did increased the
content loss value by one order of magnitude.

\input{content/experiments/table_layer_number}

\subsection{Network pre-training}

Attempting a style transfer with an untrained neural network generate an
unsuccessful result like the one present in \autoref{fig:pre-training}.

\begin{figure}
    \centering
    \includegraphics[width=.3\textwidth]{turtle/kanagawa/1_B5_L2/5_B12345_L11111/False/5/9.9000e-01/9.9900e-01/1.0000e-07/False/1.0000e+03/1.0000e-02/1000/turtle_kanagawa.png}
    \caption{Neural Style Transfer attempt with an untrained Neural Network}
    \label{fig:pre-training}
\end{figure}

\subsection{Optimization parameters}

% \paragraph{Learning rate}

\paragraph{Exponential decay rate}

\autoref{table:beta} shows the effect of the exponential decay rates for first
and second moment, $\beta_1$ and $\beta_2$ respectively. From the loss versus
iteration plots we can conclude that a not large enough $\beta_1$ will cause
stability issues in the convergence. A not large enough $\beta_2$ can make
divergent the behaviour of our loss function. Larger decay rates lead then to a
faster convergence. But a value for $\beta_1$ larger than the default behaves
worse in terms of the output image. This proves appropriate our choice of
default values for the parameters $\beta_1$ and $\beta_2$ described in
\autoref{table:defaults}.

\input{content/experiments/table_beta}

\paragraph{Epsilon}

Changes in the epsilon hyperparameter did not cause any major difference
in the performance of the network.

\subsection{Loss function weights}
Recalling \autoref{eqn:content-loss}, there are parameters that can be used to
experiment with the network in terms of the loss calculation. The content loss
weight $\alpha$ alpha and style loss weight $\beta$ will give different
importance to the content and style component respectively. In the loss plot,
higher $\alpha$ with lower $\beta$ will lead to the total loss being close to
the content loss, while lower $\alpha$ and higher $\beta$ will bring the total
loss and style loss closer in value. 

\input{content/experiments/table_loss_weights}

One can see in \autoref{table:loss-weights} that drastic alterations on content
and style loss have little effect on the result image if the number of
iterations is constant. But this parameters will affect the convergence
algorithm. Our convergence algorithm checks how close are content and style
loss and this weights will change how many iterations will be needed for the
algorithm to determine that the convergence has been achieved. In
\autoref{table:loss-weights}, only the low style weight and high content weight
combination lets us identify a convergence in the loss versus iteration plot.
It is no coincidence that this values are close to the default parameters used
in the rest of the experiments (See \autoref{table:defaults}).

\subsection{Blurred images}
Real time style transfer might face the problem of blurred images. We assessed
therefore the effects of blurred images in both content and style images as a
viability check for the real time style transfer.

Our default content and style images have resolution of 3367x2525 pixels and
4335x2990 pixels respectively. Three gaussian kernels of different size will be
applied to the images. We have small, medium and big kernel size being them
approximately 0.1\%, 1\% and 10\% of the image height respectively.
\autoref{table:blur} shows the result of the comparison.

Applying a small gaussian kernel to an image will result in smoothing the edges
with a light blur effect. This does not noticeably affect the output result.
However when using a larger gaussian filter on the content layer, and keeping
the style layer with a small kernel, the content features are almost entirely
gone but the output image still captures the style nicely. The scenario where
the style image is highly blurred is by far the worse as the blurred style
propagates to the whole image. This suggests us that real time style transfer
with motion blurred images would be viable as long as the style is not blurred.

\input{content/experiments/table_blur}

\subsection{Resolution of images}
Real time style transfer needs to be fast. Therefore we assessed the impact of
lowering the resolution of the images. In \autoref{table:resolution} one can
see that a low content resolution can lead to acceptable results as long as the
style resolution is big enough. Close to the blurred images scenario as one
might expect.

Unfortunately, a reducing by a factor of 4 the content image width and height
meant only a reduction of $0.24\%$ in the iteration calculation time for our
setup. Therefore this will not solve the time constraints of real time style transfer.

\input{content/experiments/table_resolution}

\subsection{Different content and styles}

Lastly, different contents and styles were evaluated.
\autoref{table:experiments_content_style} shows a small sample of the dataset
used. The full dataset can be found hosted in
\href{https://gitlab.com/jemaro/wut/neural-networks/style-transfer/-/tree/master/datasets/style_transfer}{a
git repository}. The source of all the dataset images is
\href{https://commons.wikimedia.org/wiki/Main_Page}{Wikimedia Commons} and it
was evaluated making use of the convergence detection described in
\autoref{sec:convergence-detection}.

\input{content/experiments/table_content_style}

One can find in \autoref{table:highlighted-contents-and-styles} our favourite
style transfers from our experiments.

\input{content/experiments/table_highlight}